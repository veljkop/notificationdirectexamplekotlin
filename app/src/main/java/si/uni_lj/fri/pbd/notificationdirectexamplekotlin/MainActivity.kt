package si.uni_lj.fri.pbd.notificationdirectexamplekotlin

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.RemoteInput
import androidx.core.content.ContextCompat
import si.uni_lj.fri.pbd.notificationdirectexamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    companion object{
        const val TAG = "MainActivity"
        const val CHANNELID = "si.uni_lj.fri.pbd.notificationdirectexamplekotlin.NEWS"
        const val NOTIFICATIONID = 101
        const val KEY_TEXT_REPLY = "key_text_reply"
        const val NOTIF_REQUEST_CODE = 42

    }

    private var notificationManager: NotificationManagerCompat? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        notificationManager = NotificationManagerCompat.from(this)

        // TODO: create channel

        // TODO: call handleIntent()
    }

    // TODO: create notification channel, if SDK>=26
    private fun createChannel(id: String, name: String, desc: String) {

    }


    // TODO: handle notification (with action) sending
    fun handleSending(){

    }


    fun sendNotification(view: View){

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(TAG, "Permission to post notification denied")

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.POST_NOTIFICATIONS)){

                val builder = AlertDialog.Builder(this)
                with(builder){
                    setMessage("I need a mic to hear your beautiful singing!")
                    setTitle("Permission I really need")
                    setPositiveButton("OK"){p0, p1->
                        makeRequest()
                    }
                }

                val dialog = builder.create()
                dialog.show()

            } else {
                makeRequest()
            }

        } else {
            handleSending()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.POST_NOTIFICATIONS), NOTIF_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission was denied")
        } else {
            Log.d(TAG, "Permission was granted")
            handleSending()
        }
    }

    // TODO: handle the notif intent and update UI
    private fun handleIntent() {

    }

}